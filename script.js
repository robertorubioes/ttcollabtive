$(document).ready(function() {
   
   
   
    //cargamos el objeto config
   document.config = config = {
       
       username:"",
       pass:"",
       url:"",
       id:"",
       
       set:function(username,pass,url,id){
           
           this.url         =   url;
           this.username    =   username;
           this.pass        =   pass;
           this.id          =   id;
            
       },
       get:function(){
           return {'username':this.username,'pass':this.pass,'url':this.url,'id':this.id};
       }
   }
   
   
   //cargamos Projects en la select de projects

  document.getProjects = getProjects = function getProjects(){
        
        //limpiamos
        $("#project").html("");
        //extraemos los proyectos for
        $.get(config.url+"api.php?action=myprojects.get&id="+config.id+"&username="+config.username+"&pass="+config.pass+"&mode=json",
           function(data){
                var i = 0;
                
                       
                for (x in data)
                {      
                       $("#project").append("<option id='"+data[i].ID+"'>"+data[i].name+"</option>");
                       i ++;
                }
                //extraemos tareas. for
                
           }, "json").error(function() {
               
               $('#tt').hide();$('#configuration').show();
               message("4");
               
           });
   }
   
  
   //cargamos tareas en la select de Tareas
   document.getTasks = getTasks = function getTasks(){
       
       //limpiamos
       $("#ttask").html("");            
        $.get(config.url+"api.php?action=user.tasks.get&user="+config.id+"&username="+config.username+"&pass="+config.pass+"&mode=json",
           function(data){
                var i = 0;
                $("#ttask").append("<option id=''>Sin tarea asociada</option>");
                       
                for (x in data)
                {      
                       $("#ttask").append("<option id='"+data[i].ID+"'>"+data[i].name+"</option>");
                       i ++;
                }
                //extraemos tareas. for
                
           }, "json").error(function() {
               
               $('#tt').hide();$('#configuration').show();
               message("4");
               
           });
         
   }
  
   function load(){
      
        
     //las metemos en las variables     
     
     var username   = getCookie("username");
     var pass       = getCookie("pass");
     var url        = getCookie("url");
     var id         = getCookie("id");
     var trackcomm  = getCookie("trackcomm");
     var ended      = getCookie("ended");
     var started    = getCookie("started");
     
     //cargamos en las cookies
     config.set(username,pass,url,id);     
     
     //introducimos los inputs
     $("#url").val(url);
     $("#username").val(username);
     $("#pass").val(pass);
     
     var now = new Date();
     
     
     $("#ttday").val(now.format("dd.mm.yyyy"));
     //datos del track, por si hubieran 
     $("#started").val(started);
     $("#ended").val(ended);
     $("textarea#trackcomm").val(trackcomm);
     
     //cargsmos los projectos y las tareas del usuario.
     getProjects();
     getTasks();
      
     //comprobamos si tiene seleccionado y introducido datos en tarea e inicializamos los inputs. meterle el selected
     
   }
   
document.message = message = function message(id){
       
       $("#message").show();
       switch(id){
           
           case '1':text = "data access."; break;
           case '2':text = "must be all data"; break;
           case '3': text = "data saved";break;
           case '4': text = "The API is deactivated. The API does not properly enforce permissions yet. For example every user can request every project / task etc.To activate the API remove the die() statement in api.php line 38.DO NOT USE THE API ON PRODUCTION SERVERS ! <br/>url or login/pass error comprobate";break;
           case '5': text = "save time Tracke successfull";break;
           case '6': text = "start hour minus that end hour";break;
           default: text = ""; $("#message").hide(); break;    
               
       } 
       
       $("#message").html(text);          
   }
   
document.islogged = islogged = function islogged(){
        
     if(getCookie("username")){     
            return true;    
     }else{
            //Extraemos las cookies guardads
            return false;
     }
     
   }
// GEstion de cookies
document.getCookie = getCookie = function getCookie(c_name)
{
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
      x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
      y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
      x=x.replace(/^\s+|\s+$/g,"");
      if (x==c_name)
        {
        return unescape(y);
        }
      }
      
      return false;
}

document.setCookie = setCookie = function setCookie(c_name,value)
{
    var exdate=new Date();
    var exdays = "2000000";
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}
   
   
if(islogged()){
       
       load(); 
       $("#configuration").hide();
       $("#tt").show();
       
       
   }else{
       
       message(1);
       $("#configuration").show();
       $("#tt").hide();
       
   }
   
});



   $('.go_to.config').click(function(){$('#tt').hide();$('#configuration').show();});
   
   $('.go_to.track').click(function(){
       
       if(document.islogged()){
                $('#configuration').hide();$('#tt').show();
           }else{
                message("2");
           }});
   
   
   function setDateTrack(){
       
       document.setCookie('started',$('#started').val());
       document.setCookie('ended',$('#ended').val());
       document.setCookie('trackcomm',$('#trackcomm').val());
   }
   
   
   $('.dset').change(function(){

       setDateTrack();
       
   });


   //enviamos el timetracker
    $('#submitTT').click(function() {
        
        if($("#started").val() >= $("#ended").val()){
            
            document.message("6");  
            
        }else{
        
            //primero hacemos login 
            $.post(config.url+'manageuser.php?action=login', { username: config.username, pass: config.pass }

            );

            //enviamos track
            $.post(config.url+'managetimetracker.php?action=add', {'started':$('#started').val(),'ended':$('#ended').val(),'project':$('#project option:selected').attr('id'),'ttask':$('#ttask option:selected').attr('id'),'comment':$('textarea#trackcomm').val()},
            function(){

                //Limpiamos los datos de las cookies jur.
                document.setCookie('started',"");
                document.setCookie('ended',"");
                document.setCookie('trackcomm',"");
                document.message("5");
                $("#started").val("");
                $("#ended").val("");
                $("textarea#trackcomm").val("");
               
            });
           
        }

    });

$("#start").click(function(){    
    var now = new Date();$('#started').val(now.format('HH:MM'));  
    setDateTrack();
});

$("#finish").click(function(){
    var now = new Date();$('#ended').val(now.format('HH:MM'));   
    setDateTrack();
});


    //guardamos la configuración
    $('#submitConfig').click(function() {

          // cargamos del form 
          var username = $("#username").val();
          var pass = $("#pass").val();
          var url = $("#url").val();
          
          
           //Extraemos el identificador del usuario. 
           $.get(config.url+"api.php?action=user.list.get&username="+username+"&pass="+pass+"&mode=json",
           function(data){
               
                var i = 0;
                for (x in data)
                {      
                    
                     if(data[i].name == username ){
                        
                         document.config.id = data[i].ID;
                         break;
                     }
                     i++;
                }
               
                
           }, "json").error(function() {
               
                message("3");
                $("#configuration").hide();
                $("#tt").show();
                
           });
          
          
         

          // comprobamos que los datos estan introducidos
          if (!username || !pass || !url ) {
            //mostramos mensaje    
            message("2");
            return;

          }else{
              
            // Guardamos datos en cookie
            setCookie('username', username);
            setCookie('pass',pass);
            setCookie('url',url);
            
            //Tenemos que encontrar el id del user.
            setCookie('id',document.config.id);
            document.config.set(username,pass,url,document.config.id);
            
            //Guardamos datos del track
            setCookie('started',$('#started').val());
            setCookie('ended',$('#ended').val());
            setCookie('trackcomm',$('textarea#trackcomm').val());
            //seteamos el config.
            getTasks();
            getProjects();
            message("3");
            $("#configuration").hide();
            $("#tt").show();
            
          }
    });
    
    
    
    
    //Date format fuction formated dates
    var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};